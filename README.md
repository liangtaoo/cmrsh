# Linear Subspace Ranking Hashing for Cross-modal Retrieval #

**This is  the source code of the following papers. If you use this code in your research, please cite our paper**

Kai Li, Guojun Qi, Jun Ye and Kien A. Hua, "Linear Subspace Ranking Hashing for Cross-modal Retrieval," IEEE Transactions on Pattern Analysis and Machine Intelligence (TPAMI), September 2016.

The paper can be downloaded [here](http://www.eecs.ucf.edu/~kaili/pdfs/paper7.pdf). The file lsrh_demo.m shows how to use the code.

## Notes ##

The paper is an improved version of the technique presented in the following paper: 

Kai Li, Guojun Qi, Jun Ye and Kien A. Hua, "Cross-modal Hashing Through Ranking Subspace Learning," IEEE International Conference on Multimedia and EXPO (IEEE ICME) , July 2016.

which can be downloaded [here](http://www.eecs.ucf.edu/~kaili/pdfs/paper6.pdf). 

## Copyright ##

Copyright (c) Kai Li. 2016.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.