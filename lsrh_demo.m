Nb = 32; % code length (in binary bits)
opts_K = [4]; % options for subspace dimension K
opts_lambda = [0.2 0.5 1 2]; % options for relative penalty
opts_beta = [0.1 0.5 1 2 4]; % options for softness (alpha in the paper)
%% Step 1: Load and preprocess data
dat = load('wiki_data/raw_features.mat');
ttlabels = textscan(fopen('wiki_data/testset_txt_img_cat.list'), '%s %s %u8'); 
tnlabels = textscan(fopen('wiki_data/trainset_txt_img_cat.list'), '%s %s %u8'); 

Ytr = dat.I_tr';
Xtr = dat.T_tfidf_tr';
Yte = dat.I_te';
Xte = dat.T_tfidf_te';
ttlabels = ttlabels{1, 3};
tnlabels = tnlabels{1, 3};

% center, then normalize data
mX = mean(Xtr,2);
Xtr = bsxfun(@minus, Xtr, mX);
normX = sqrt(sum(Xtr.^2, 1));
Xtr = bsxfun(@rdivide, Xtr, normX);       

Xte = bsxfun(@minus, Xte, mX);
normXte = sqrt(sum(Xte.^2, 1));
Xte = bsxfun(@rdivide, Xte, normXte);

mY = mean(Ytr,2);
Ytr = bsxfun(@minus, Ytr, mean(Ytr,2));
normY = sqrt(sum(Ytr.^2, 1));
Ytr = bsxfun(@rdivide, Ytr, normY);    

Yte = bsxfun(@minus, Yte, mY);
normYte = sqrt(sum(Yte.^2, 1));
Yte = bsxfun(@rdivide, Yte, normYte);    

train_data.X = Xtr;
train_data.Y = Ytr;
query_data.X = Xte;                  
query_data.Y = Yte;

train_data.S = uint8(bsxfun(@eq, tnlabels, tnlabels'));
query_data.S = uint8(bsxfun(@eq, ttlabels, tnlabels'));    

%% Step 2: Cross-validation   
args = lsrh_cv(train_data, Nb, opts_K, opts_lambda, opts_beta);
        
opts.lambda = args.lambda;
opts.beta = args.beta;
opts.K = args.K;
opts.L = ceil(Nb / ceil(log2(args.K))); % train maximum number of bits     

%% Step 3: Training LSRH models
fprintf(' ## LSRH training, L = : %d (K = %d)\n', opts.L, opts.K);        
[~, model] = lsrh(train_data, opts);

%% Step 4: Encode to get query and database codes
fprintf(' ## LSRH cncoding ...\n');           
[test_code, ~] = lsrh(query_data, opts, model);        
[db_code, ~] = lsrh(train_data, opts, model);

Hxt = test_code.Hx; Hyt = test_code.Hy;
Hxdb = db_code.Hx; Hydb = db_code.Hy;

%% Step 5: Compute mean average precision
fprintf(' ## LSRH computing mAP ...\n');   
DxyTestDb = pdist2(Hxt', Hydb', 'hamming')*opts.L;
[~, mAPxy, ~, ~, ~] = test_hash(DxyTestDb, query_data.S, opts.L, []);           
DyxTestDb = pdist2(Hyt', Hxdb', 'hamming')*opts.L;
[~, mAPyx, ~, ~, ~] = test_hash(DyxTestDb, query_data.S, opts.L, []);

fprintf(' ## LSRH DONE. mAPxy %.4f/mAPyx %.4f.\n', mAPxy, mAPyx);                       
        